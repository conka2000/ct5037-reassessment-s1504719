// This file's header.
#include "Patrol.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

static const int g_xNUM_OF_CORNERS = 4;
static const glm::vec3 g_xCORNER_POSITIONS[ g_xNUM_OF_CORNERS ] =
{
	glm::vec3( 20.0f, 0.0f, 20.0f ),
	glm::vec3( -20.0f, 0.0f, 20.0f ),
	glm::vec3( -20.0f, 0.0f, -20.0f ),
	glm::vec3( 20.0f, 0.0f, -20.0f ),
};

Patrol::Patrol( AIBrain* pxOwnerBrain )
	: Action( pxOwnerBrain )
	, m_iCurrentCornerIndex( 0 )
{
}

NODE_STATUS Patrol::Update()
{
	if( !GetOwnerBrain() || !GetOwnerBrain()->GetOwnerActor() )
	{
		return FAILURE;
	}

	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	if( !m_bIsMoving )
	{
		pxOwnerActor->RequestMove( g_xCORNER_POSITIONS[ m_iCurrentCornerIndex ] );
		m_bIsMoving = true;
	}
	else if( MyApplication::FindDistanceBetween( pxOwnerActor->GetCurrentPosition(), g_xCORNER_POSITIONS[ m_iCurrentCornerIndex ] ) < 1.5f )
	{
		// Stop.
		pxOwnerActor->RequestStop();

		// Unflag that we are moving.
		m_bIsMoving = false;

		// Increment the corner index.
		++m_iCurrentCornerIndex;

		if( m_iCurrentCornerIndex >= g_xNUM_OF_CORNERS )
		{
			// If we have reached the final corner, success.
			m_iCurrentCornerIndex = 0;
			return SUCCESS;
		}
	}

	return RUNNING;
}