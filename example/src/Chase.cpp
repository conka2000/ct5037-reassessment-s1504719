// This file's header.
#include "Chase.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

Chase::Chase(AIBrain* pxOwnerBrain)
	: Action(pxOwnerBrain)
{
}

NODE_STATUS Chase::Update()
{
	if (!GetOwnerBrain() || !GetOwnerBrain()->GetOwnerActor())
	{
		return FAILURE;
	}

	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	//Move in direction to enemy
    pxOwnerActor->RequestMove(pxOwnerActor->GetEnemyActor()->GetCurrentPosition());

	//If close to enemy then tag them
	float dist = MyApplication::FindDistanceBetween(pxOwnerActor->GetCurrentPosition(), pxOwnerActor->GetEnemyActor()->GetCurrentPosition());
	if (dist < 2.f)
	{
		pxOwnerActor->Tagged();
		pxOwnerActor->GetEnemyActor()->Tagged();
		return FAILURE;
	}
	return SUCCESS;
}