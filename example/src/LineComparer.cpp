#include "LineComparer.h"
#include "Gizmos.h"
#include "MyApplication.h"

LineComparer::LineComparer()
{

}

LineComparer::~LineComparer()
{

}
//Checks if three points are listed in a counterclockwise order
bool LineComparer::CCW(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3)
{
	return (point3.z - point1.z) * (point2.x - point1.x) > (point2.z - point1.z) * (point3.x - point1.x);
}

//Checks if lines intersect
bool LineComparer::LinesIntersect(glm::vec3 line1point1, glm::vec3 line1point2, glm::vec3 line2point1, glm::vec3 line2point2)
{
	return CCW(line1point1, line2point1, line2point2) != CCW(line1point2, line2point1, line2point2) && CCW(line1point1, line1point2, line2point1) != CCW(line1point1, line1point2, line2point2);
}

