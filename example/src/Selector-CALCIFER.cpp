// This file's header.
#include "Selector.h"

Selector::Selector()
{
	m_iCurrentIndex = 0;
}

NODE_STATUS Selector::Update()
{
	NODE_STATUS eReturnStatus = FAILURE;

	Node* pxCurrentNode = GetChildNodes()[ m_iCurrentIndex ];
	if( pxCurrentNode )
	{
		NODE_STATUS eStatus = pxCurrentNode->Update();
		if( eStatus == FAILURE )
		{
			if( m_iCurrentIndex == ( GetChildNodes().size() - 1 ) )
			{
				eReturnStatus = FAILURE;
			}
			else
			{
				++m_iCurrentIndex;
				eReturnStatus = RUNNING;
			}
		}
		else
		{
			eReturnStatus = eStatus;
		}

		if( eReturnStatus == SUCCESS
			|| eReturnStatus == FAILURE )
		{
			Reset();
		}		
	}

	return eReturnStatus;
}