// This files header.
#include "AIBrain.h"

// Other includes.
#include "AIActor.h"
#include "TagBT.h"

AIBrain::AIBrain( AIActor* pxOwnerActor )
	: m_pxOwnerActor( pxOwnerActor )
	, m_pxCurrentBT( new TagBT(this) )
{
	m_pxCurrentBT->Initialise();
}

AIBrain::~AIBrain()
{
	delete m_pxCurrentBT;
}

void AIBrain::Update(float a_deltaTime)
{
	m_pxCurrentBT->Update();
}