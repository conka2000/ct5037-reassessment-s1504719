// This file's header.
#include "Seek.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

Seek::Seek(AIBrain* pxOwnerBrain)
	: Action(pxOwnerBrain)
{
}

NODE_STATUS Seek::Update()
{
	if (!GetOwnerBrain() || !GetOwnerBrain()->GetOwnerActor())
	{
		return FAILURE;
	}

	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	//Get enemy's position
	glm::vec3 enemyPos = pxOwnerActor->GetEnemyActor()->GetCurrentPosition();

	//If not moving
	if (!m_bIsMoving)
	{		
		int myID = pxOwnerActor->GetCurrentNodeID();

		int otherID;

		float minDistance = 1000;

		//Convert neighours vector to array
		glm::vec4 myNeighboursV = pxOwnerActor->GetNavMesh()->GetNeighboursOf(myID);
		int myNeighbours[4] = { myNeighboursV.r, myNeighboursV.g, myNeighboursV.b, myNeighboursV.a };

		//Check all neighbours for closest to enemy
		for (int i = 0; i < pxOwnerActor->GetNavMesh()->GetNeighbourCountOf(myID); i++)
		{
			if (myNeighbours[i] == pxOwnerActor->GetEnemyCurrentNodeID())
			{
				otherID = myNeighbours[i];
			}
			else
			{
				float dist = MyApplication::FindDistanceBetween(enemyPos, pxOwnerActor->GetNavMesh()->GetPointToNextNode(myID, myNeighbours[i]));
				if (dist < minDistance && myNeighbours[i] != pxOwnerActor->GetPreviousNode())
				{
					minDistance = dist;
					otherID = myNeighbours[i];
				}
			}
		}

		//Go to point for entry to next node
		goToPos = pxOwnerActor->GetNavMesh()->GetPointToNextNode(myID, otherID);
		if (goToPos.y >= 100)
		{
			otherID = pxOwnerActor->GetNavMesh()->GetNeighboursOf(myID).y;
			goToPos = pxOwnerActor->GetNavMesh()->GetPointToNextNode(myID, otherID);
		}

		//Record current node so not to get stuck in a loop
		pxOwnerActor->SetPreviousNode(myID);
		goToNode = otherID;

		//Move
		pxOwnerActor->RequestMove(goToPos);
		m_bIsMoving = true;
	}
	else if ( //If close enough to node's center and on the node
		MyApplication::FindDistanceBetween(pxOwnerActor->GetCurrentPosition(), goToPos) < 10.f
		&&
		pxOwnerActor->GetNavMesh()->GetNodeQuadIDOfPoint(pxOwnerActor->GetCurrentPosition()) == goToNode
		)
	{
		//Stop moving
		pxOwnerActor->RequestStop();
		m_bIsMoving = false;
		return SUCCESS;
	}
	return RUNNING;
}