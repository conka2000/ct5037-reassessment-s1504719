// This file's header.
#include "Flee.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

Flee::Flee(AIBrain* pxOwnerBrain)
	: Action(pxOwnerBrain)
{
}

NODE_STATUS Flee::Update()
{
	if (!GetOwnerBrain() || !GetOwnerBrain()->GetOwnerActor())
	{
		return FAILURE;
	}

	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	//Record current position
	glm::vec3 myPos = pxOwnerActor->GetCurrentPosition();

	//If not moving
	if (!m_bIsMoving)
	{
		int myID = pxOwnerActor->GetCurrentNodeID();

		int otherID;

		float maxDistance = 0;

		//Convert neighbours vector to array
		glm::vec4 myNeighboursV = pxOwnerActor->GetNavMesh()->GetNeighboursOf(myID);
		int myNeighbours[4] = { myNeighboursV.r, myNeighboursV.g, myNeighboursV.b, myNeighboursV.a };

		//Get node furthest from self
		for (int i = 0; i < pxOwnerActor->GetNavMesh()->GetNeighbourCountOf(myID); i++)
		{
			float dist = MyApplication::FindDistanceBetween(myPos, pxOwnerActor->GetNavMesh()->GetPointToNextNode(myID, myNeighbours[i]));
			if (dist > maxDistance && myNeighbours[i] != pxOwnerActor->GetPreviousNode())
			{
				maxDistance = dist;
				otherID = myNeighbours[i];
			}
		}

		//Go to the position
		goToPos = pxOwnerActor->GetNavMesh()->GetPointToNextNode(myID, otherID);
		if (goToPos.y >= 100)
		{
			otherID = pxOwnerActor->GetNavMesh()->GetNeighboursOf(myID).y;
			goToPos = pxOwnerActor->GetNavMesh()->GetPointToNextNode(myID, otherID);
		}

		//Record current node and node going to
		pxOwnerActor->SetPreviousNode(myID);
		goToNode = otherID;

		//Move
		pxOwnerActor->RequestMove(goToPos);
		m_bIsMoving = true;
	}
	else if (//If close enough and on node then stop
		MyApplication::FindDistanceBetween(pxOwnerActor->GetCurrentPosition(), goToPos) < 2.f
		&&
		pxOwnerActor->GetNavMesh()->GetNodeQuadIDOfPoint(pxOwnerActor->GetCurrentPosition()) == goToNode
		)
	{
		pxOwnerActor->RequestStop();
		m_bIsMoving = false;
		return SUCCESS;
	}

	return RUNNING;
}