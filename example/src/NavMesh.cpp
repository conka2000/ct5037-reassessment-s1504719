#include "NavMesh.h"
#include "Gizmos.h"
#include "MyApplication.h"

NavMesh::NavMesh()
{
	OnCreate();
}

NavMesh::~NavMesh()
{

}

void NavMesh::OnCreate()
{
	
}

void NavMesh::Update(float a_deltaTime)
{

}

//Adds new node to NavMesh
void NavMesh::AddQuadNode(glm::vec3 q_Vertices[4])
{
	//Intialise new node
	NavNodeQuad newNode;
	newNode.navMeshID = nodeCount;
	int newID = newNode.navMeshID;
	//Calculate position
	newNode.vertices[0] = q_Vertices[0];
	newNode.vertices[1] = q_Vertices[1];
	newNode.vertices[2] = q_Vertices[2];
	newNode.vertices[3] = q_Vertices[3];
	newNode.position = (q_Vertices[0] + q_Vertices[1] + q_Vertices[2] + q_Vertices[3]) * 0.25;

	bool alreadyMatched;
	//Copy current node list
	std::deque<NavNodeQuad> copyQueue = myNodes;

	if (nodeCount > 0)
	{
		//Take out nodes from list and check them to see if they are neighbours to new node
		while(!myNodes.empty())
		{
			alreadyMatched = false;
			//Take out node
			NavNodeQuad testingNode = myNodes.at(myNodes.size() - 1);	
			myNodes.pop_back();
			copyQueue.pop_back();
			int testID = testingNode.navMeshID;
			for (int j = 0; j < 4; j++)
			{
				for (int k = 0; k < 4; k++)
				{
					for (int l = 0; l < 4; l++)
					{
						for (int m = 0; m < 4; m++)
						{
							//Check all lines to see if any intersect
							if (lineComparer.LinesIntersect(newNode.vertices[j], newNode.vertices[k], testingNode.vertices[l], testingNode.vertices[m]))
							{
								//Check we're not adding the same node as a neighbour twice
								for (int o = 0; o < newNode.neighbourCount; o++)
								{
									if (newNode.neighbours[o] == testID)
									{
										alreadyMatched = true;
									}
								}
								if (!alreadyMatched)
								{
									//Register nodes as neighbours
									newNode.neighbourCount++;
									testingNode.neighbourCount++;
									if (newNode.neighbourCount > 4)
									{
										newNode.neighbourCount = 4;
									}
									if (testingNode.neighbourCount > 4)
									{
										testingNode.neighbourCount = 4;
									}
									newNode.neighbours[newNode.neighbourCount - 1] = testID;
									testingNode.neighbours[testingNode.neighbourCount - 1] = newID;									
								}
							}
						}
					}
				}
			}
			//Put pulled out node back at start
			copyQueue.push_front(testingNode);
		}
	}
	//Insert new node
	copyQueue.push_back(newNode);
	//Replace list
	myNodes = copyQueue; 
	//Record node count
	nodeCount++;
}

//Finds the node of which a point lies on and returns its ID
//Works for non diagonal rectangles
int NavMesh::GetNodeQuadIDOfPoint(glm::vec3 p_Position)
{
	//Returns -1 if no node is found
	int returnNumber = -1;
	//Copy node list
	std::deque<NavNodeQuad> copyQueue = myNodes;
	//Take out nodes until a node with the point is found
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		//Record X and Z values to get the rectangular shape of the node
		int leastX = testNode.vertices[0].x;
		int mostX = testNode.vertices[0].x;
		for (int i = 1; i < 4; i++)
		{
			if (testNode.vertices[i].x < leastX)
			{
				leastX = testNode.vertices[i].x;
			}
			else if (testNode.vertices[i].x > mostX)
			{
				mostX = testNode.vertices[i].x;
			}
		}

		int leastZ = testNode.vertices[0].z;
		int mostZ = testNode.vertices[0].z;
		for (int i = 1; i < 4; i++)
		{
			if (testNode.vertices[i].z < leastZ)
			{
				leastZ = testNode.vertices[i].z;
			}
			else if (testNode.vertices[i].z > mostZ)
			{
				mostZ = testNode.vertices[i].z;
			}
		}

		//Is position in the rectangle?
		if (p_Position.x >= leastX && p_Position.x <= mostX && p_Position.z >= leastZ && p_Position.z <= mostZ)
		{
			//Return its ID
			returnNumber = testNode.navMeshID;
			break;
		}
	}
	//Return -1 if no node found
	return returnNumber;
}

//Returns a vector of a nodes neighbours' IDs
glm::vec4 NavMesh::GetNeighboursOf(int navID)
{
	//Default return
	glm::vec4 returnVec = glm::vec4(-2,-2,-2,-2);
	//Copy list
	std::deque<NavNodeQuad> copyQueue = myNodes;
	//Pull out nodes to identify the node we are looking at
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		if (testNode.navMeshID == navID)
		{
			//Return the nodes vectors
			returnVec = glm::vec4(testNode.neighbours[0], testNode.neighbours[1], testNode.neighbours[2], testNode.neighbours[3]);
			break;
		}
	}
	//Return default if node doesnt exist
	return returnVec;
}

//Returns the amount of neighbours a node has
int NavMesh::GetNeighbourCountOf(int navID)
{
	int defaultReturn = -1;
	//Copy list
	std::deque<NavNodeQuad> copyQueue = myNodes;
	//Pull out nodes to identify the node we are looking at
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		if (testNode.navMeshID == navID)
		{
			//Return the neighbour count
			return testNode.neighbourCount;
			break;
		}
	}
	return defaultReturn;
}

//Returns node's position to go to for navigating to a neighbouring node
//Returns (0,100,0) if other node is not a neighbour
glm::vec3 NavMesh::GetPointToNextNode(int myID, int otherID)
{
	NavNodeQuad myNode;
	NavNodeQuad otherNode;
	std::deque<NavNodeQuad> copyQueue = myNodes;
	//Identify nodes
	while (!copyQueue.empty())
	{
		NavNodeQuad testNode = copyQueue.at(copyQueue.size() - 1);
		copyQueue.pop_back();
		if (testNode.navMeshID == myID)
		{
			myNode = testNode;
		}
		else if (testNode.navMeshID == otherID)
		{
			otherNode = testNode;
		}

		if (myNode.navMeshID > -1 && otherNode.navMeshID > -1)
		{
			break;
		}
	}

	for (int i = 0; i < myNode.neighbourCount; i++)
	{
		if (myNode.neighbours[i] == otherID)
		{
			//Return other node's position
			return otherNode.position;
		}
	}
	
	return glm::vec3(0,100,0);
}
