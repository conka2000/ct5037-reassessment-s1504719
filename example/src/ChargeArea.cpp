#include "ChargeArea.h"
#include "Gizmos.h"
#include "AIActor.h"


ChargeArea::ChargeArea()
	: m_xPosition(0.f, 0.1f, 0.f)
	, m_xColour(1.0f, 0.0f, 0.0f, 1.0f)
{
}


ChargeArea::~ChargeArea()
{
}

void ChargeArea::Update()
{
	Gizmos::addCylinder(m_xPosition, 1.5f, 0.1f, 50, true, m_xColour);
}
