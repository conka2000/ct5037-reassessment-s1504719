// This file's header.
#include "Recharge.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

Recharge::Recharge( AIBrain* pxOwnerBrain )
	: Action( pxOwnerBrain )
{
}

NODE_STATUS Recharge::Update()
{
	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	if( !m_bIsMoving )
	{
		pxOwnerActor->RequestMove( MyApplication::s_xChargeArea.GetPosition() );
		m_bIsMoving = true;
	}
	else if( MyApplication::FindDistanceBetween( pxOwnerActor->GetCurrentPosition(), MyApplication::s_xChargeArea.GetPosition() ) < 1.0f )
	{
		pxOwnerActor->RequestStop();
		m_bIsMoving = false;
		return SUCCESS;
	}

	return RUNNING;
}