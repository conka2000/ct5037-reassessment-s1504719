// This file's header.
#include "IsEnemyInSight.h"

// Other headers.
#include "AIBrain.h"
#include "AIActor.h"

IsEnemyInSight::IsEnemyInSight(AIBrain* pxOwnerBrain) : Condition(pxOwnerBrain)
{
	//Record lines of walls
	for (int i = 0; i < 20; i++)
	{ 
		pxWallLines[i] = mapStage.GetWallLines()[i];
	}
}

//Changes state if enemy should be seen but is not and vice versa
NODE_STATUS IsEnemyInSight::Update()
{
	if (GetOwnerBrain() && GetOwnerBrain()->GetOwnerActor())
	{
		AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

		//Record positions
		glm::vec3 myPosition = pxOwnerActor->GetCurrentPosition();
		glm::vec3 enemyPosition = pxOwnerActor->GetEnemyActor()->GetCurrentPosition();

		//Record node IDs
		int myID = pxOwnerActor->GetCurrentNodeID();
		int otherID = pxOwnerActor->GetEnemyCurrentNodeID();

		//If on the same node they obviously can see eachother
		if (myID == otherID)
		{
			if (pxOwnerActor->GetCurrentState() == SEEK)
			{
				pxOwnerActor->RequestStop();
				pxOwnerActor->SetState(CHASE);
			}
			else if (pxOwnerActor->GetCurrentState() == FLEE)
			{
				pxOwnerActor->RequestStop();
				pxOwnerActor->SetState(RUNAWAY);
			}
			return SUCCESS;			
		}
		else
		{
			//For all walls
			for (int i = 0; i < 24; i++)
			{
				//For the last line of each wall
				if (i == 3 || i == 7 || i == 11 || i == 15 || i == 19)
				{
					if (lineComparer.LinesIntersect(myPosition, enemyPosition, pxWallLines[i], pxWallLines[i - 3]))
					{
						if (pxOwnerActor->GetCurrentState() == CHASE)
						{
							pxOwnerActor->RequestStop();
							pxOwnerActor->SetState(SEEK);
						}
						else if (pxOwnerActor->GetCurrentState() == RUNAWAY)
						{
							pxOwnerActor->RequestStop();
							pxOwnerActor->SetState(FLEE);
						}
						return FAILURE;
					}
				}
				//Check if one of the lines of the wall intersects the view between actors
				else if(lineComparer.LinesIntersect(myPosition, enemyPosition, pxWallLines[i], pxWallLines[i + 1]))
				{
					if (pxOwnerActor->GetCurrentState() == CHASE)
					{
						pxOwnerActor->RequestStop();
						pxOwnerActor->SetState(SEEK);
					}
					else if (pxOwnerActor->GetCurrentState() == RUNAWAY)
					{
						pxOwnerActor->RequestStop();
						pxOwnerActor->SetState(FLEE);
					}
					return FAILURE;
				}
			}
		}
		if (pxOwnerActor->GetCurrentState() == SEEK)
		{
			pxOwnerActor->RequestStop();
			pxOwnerActor->SetState(CHASE);
		}
		else if (pxOwnerActor->GetCurrentState() == FLEE)
		{
			pxOwnerActor->RequestStop();
			pxOwnerActor->SetState(RUNAWAY);
		}
	}

	return SUCCESS;
}