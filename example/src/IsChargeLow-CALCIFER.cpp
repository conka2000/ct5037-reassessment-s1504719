// This file's header.
#include "IsChargeLow.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"

static const float g_fMIN_CHARGE_LEVEL = 25.0f;

IsChargeLow::IsChargeLow( AIBrain* pxOwnerBrain )
	: Condition( pxOwnerBrain )
{

}

NODE_STATUS IsChargeLow::Update()
{
	if( GetOwnerBrain() && GetOwnerBrain()->GetOwnerActor() )
	{
		AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();
		if( pxOwnerActor->GetCurrentCharge() < g_fMIN_CHARGE_LEVEL )
		{
			return SUCCESS;
		}
	}

	return FAILURE;
}