// This file's header.
#include "Sleep.h"

// Other headers.
#include "AIActor.h"
#include "AIBrain.h"
#include "MyApplication.h"

#include <time.h>

Sleep::Sleep(AIBrain* pxOwnerBrain)
	: Action(pxOwnerBrain)
{
}

NODE_STATUS Sleep::Update()
{
	if (!GetOwnerBrain() || !GetOwnerBrain()->GetOwnerActor())
	{
		return FAILURE;
	}

	AIActor* pxOwnerActor = GetOwnerBrain()->GetOwnerActor();

	//Stop initially
	if (sleepTimer <= 0)
	{
		pxOwnerActor->RequestStop();
	}

	//Add time to timer
	sleepTimer += Utility::tickTimer();

	//Slept more than alloted time?
	if (sleepTimer > sleepTime)
	{
		//Wake up and chase
		sleepTimer = 0.f;
		pxOwnerActor->SetState(CHASE);
		return FAILURE;
	}

	return RUNNING;
}