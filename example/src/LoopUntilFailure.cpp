// This file's header.
#include "LoopUntilFailure.h"

LoopUntilFailure::LoopUntilFailure(Node* pxChild) : Decorator(pxChild)
{

}

NODE_STATUS LoopUntilFailure::Update()
{
	Node* pxChild = GetChildNode();
	if (pxChild)
	{
		NODE_STATUS eStatus = pxChild->Update();
		if (eStatus == SUCCESS)
		{
			return RUNNING;
		}
		else if (eStatus == FAILURE)
		{
			return FAILURE;
		}
		else
		{
			return eStatus;
		}
	}
}