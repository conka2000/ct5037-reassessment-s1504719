#include "Stage.h"

Stage::Stage()
{

}

Stage::~Stage()
{

}

// Renders all walls and the navmesh
void Stage::Update()
{
	// Procedurally render each wall in arena specifically

	// This for loop is for outside boundaries
	for (float i = -21 * square; i <= 21 * square; i++)
	{
		Gizmos::addBox(glm::vec3(i, square, -21 * square), glm::vec3(square, square * 3, square), true, wallColour);
		Gizmos::addBox(glm::vec3(i, square, 21 * square), glm::vec3(square, square * 3, square), true, wallColour);

		Gizmos::addBox(glm::vec3(-21 * square, square, i), glm::vec3(square, square * 3, square), true, wallColour);
		Gizmos::addBox(glm::vec3(21 * square, square, i), glm::vec3(square, square * 3, square), true, wallColour);
	}

	// Each for loop is a line across the arena with walls (including gaps) (horizontal or vertical)
	for (float i = -21 * square; i <= 21 * square; i++)
	{
		if ((i >= -19 * square && i <= -13 * square) || (i >= -5 * square && i <= 10 * square))
		{
			Gizmos::addBox(glm::vec3(i, square, 5 * square), glm::vec3(square, square * 2, square * 0.5), true, wallColour);
		}

		if ((i >= -15 * square && i <= -8 * square))
		{
			Gizmos::addBox(glm::vec3(-5.5 * square, square, i), glm::vec3(square  * 0.5, square * 2, square), true, wallColour);
			Gizmos::addBox(glm::vec3(-4.5 * square, square, i), glm::vec3(square  * 0.5, square * 2, square), true, wallColour);
		}

		if ((i >= 15 * square && i <= 19 * square) || (i >= -15 * square && i <= -5 * square))
		{
			Gizmos::addBox(glm::vec3(9 * square, square, i), glm::vec3(square  * 0.5, square * 2, square), true, wallColour);
		}
	}

	//Floors/NavMesh Nodes
	//Top right
	Gizmos::addTri(glm::vec3(-20, 0.01, 4.5), glm::vec3(-6, 0.01, 4.5), glm::vec3(-6, 0.01, -20), glm::vec4(0.1, 0.75, 0.1, 1));
	Gizmos::addTri(glm::vec3(-20, 0.01, -20), glm::vec3(-20, 0.01, 4.5), glm::vec3(-6, 0.01, -20), glm::vec4(0.1, 0.75, 0.1, 1));
	//Top left
	Gizmos::addTri(glm::vec3(-20, 0.01, 5.5), glm::vec3(-20, 0.01, 20), glm::vec3(8.5, 0.01, 5.5), glm::vec4(0.75, 0.1, 0.1, 1));
	Gizmos::addTri(glm::vec3(-20, 0.01, 20), glm::vec3(8.5, 0.01, 20), glm::vec3(8.5, 0.01, 5.5), glm::vec4(0.75, 0.1, 0.1, 1));
	//Bottom left
	Gizmos::addTri(glm::vec3(9.5, 0.01, 5.5), glm::vec3(9.5, 0.01, 20), glm::vec3(20, 0.01, 5.5), glm::vec4(0.1, 0.75, 0.75, 1));
	Gizmos::addTri(glm::vec3(20, 0.01, 5.5), glm::vec3(9.5, 0.01, 20), glm::vec3(20, 0.01, 20), glm::vec4(0.1, 0.75, 0.75, 1));
	//Bottom right
	Gizmos::addTri(glm::vec3(9.5, 0.01, -20), glm::vec3(9.5, 0.01, 4.5), glm::vec3(20, 0.01, -20), glm::vec4(0.75, 0.1, 0.75, 1));
	Gizmos::addTri(glm::vec3(20, 0.01, -20), glm::vec3(9.5, 0.01, 4.5), glm::vec3(20, 0.01, 4.5), glm::vec4(0.75, 0.1, 0.75, 1));
	//Middle
	Gizmos::addTri(glm::vec3(8.5, 0.01, -20), glm::vec3(-4, 0.01, -20), glm::vec3(8.5, 0.01, 4.5), glm::vec4(0.75, 0.75, 0.1, 1));
	Gizmos::addTri(glm::vec3(-4, 0.01, 4.5), glm::vec3(8.5, 0.01, 4.5), glm::vec3(-4, 0.01, -20), glm::vec4(0.75, 0.75, 0.1, 1));
	//Corridor between bottom left and right
	Gizmos::addTri(glm::vec3(11, 0.01, 5.5), glm::vec3(20, 0.01, 5.5), glm::vec3(11, 0.01, 4.5), glm::vec4(1, 1, 0.5, 1));
	Gizmos::addTri(glm::vec3(20, 0.01, 5.5), glm::vec3(20, 0.01, 4.5), glm::vec3(11, 0.01, 4.5), glm::vec4(1, 1, 0.5, 1));
	//Corridor between top left and bottom left
	Gizmos::addTri(glm::vec3(9.5, 0.01, 14), glm::vec3(9.5, 0.01, 5.5), glm::vec3(8.5, 0.01, 14), glm::vec4(1, 0.75, 1, 1));
	Gizmos::addTri(glm::vec3(8.5, 0.01, 5.5), glm::vec3(8.5, 0.01, 14), glm::vec3(9.5, 0.01, 5.5), glm::vec4(1, 0.75, 1, 1));
	//Corridor between middle and bottom right (left)
	Gizmos::addTri(glm::vec3(9.5, 0.01, 4.5), glm::vec3(9.5, 0.01, -4), glm::vec3(8.5, 0.01, 4.5), glm::vec4(0.5, 1, 1, 1));
	Gizmos::addTri(glm::vec3(8.5, 0.01, -4), glm::vec3(8.5, 0.01, 4.5), glm::vec3(9.5, 0.01, -4), glm::vec4(0.5, 1, 1, 1));
	//Corridor between middle and bottom right (right)
	Gizmos::addTri(glm::vec3(9.5, 0.01, -16), glm::vec3(9.5, 0.01, -20), glm::vec3(8.5, 0.01, -16), glm::vec4(0.5, 0.5, 1, 1));
	Gizmos::addTri(glm::vec3(8.5, 0.01, -20), glm::vec3(8.5, 0.01, -16), glm::vec3(9.5, 0.01, -20), glm::vec4(0.5, 0.5, 1, 1));
	//Corridor between middle and top right (right)
	Gizmos::addTri(glm::vec3(-4, 0.01, -16), glm::vec3(-4, 0.01, -20), glm::vec3(-6, 0.01, -16), glm::vec4(0.5, 1, 0.5, 1));
	Gizmos::addTri(glm::vec3(-6, 0.01, -20), glm::vec3(-6, 0.01, -16), glm::vec3(-4, 0.01, -20), glm::vec4(0.5, 1, 0.5, 1));
	//Corridor between middle and top right (left)
	Gizmos::addTri(glm::vec3(-4, 0.01, 4.5), glm::vec3(-4, 0.01, -7), glm::vec3(-6, 0.01, 4.5), glm::vec4(1, 0.5, 0.5, 1));
	Gizmos::addTri(glm::vec3(-6, 0.01, -7), glm::vec3(-6, 0.01, 4.5), glm::vec3(-4, 0.01, -7), glm::vec4(1, 0.5, 0.5, 1));
	//Corridor between top left and right
	Gizmos::addTri(glm::vec3(-6, 0.01, 5.5), glm::vec3(-6, 0.01, 4.5), glm::vec3(-12, 0.01, 5.5), glm::vec4(0.1, 0.1, 0.75, 1));
	Gizmos::addTri(glm::vec3(-12, 0.01, 5.5), glm::vec3(-6, 0.01, 4.5), glm::vec3(-12, 0.01, 4.5), glm::vec4(0.1, 0.1, 0.75, 1));
}