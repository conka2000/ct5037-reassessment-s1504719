// This file's header
#include "Sequence.h"

Sequence::Sequence()
{
	m_iCurrentIndex = 0;
}

NODE_STATUS Sequence::Update()
{
	NODE_STATUS eReturnStatus = FAILURE;

	Node* pxCurrentNode = GetChildNodes()[ m_iCurrentIndex ];
	if( pxCurrentNode )
	{
		NODE_STATUS eStatus = pxCurrentNode->Update();
		if( eStatus == SUCCESS )
		{
			if( m_iCurrentIndex == ( GetChildNodes().size() - 1 ) )
			{
				eReturnStatus = SUCCESS;
			}
			else
			{
				++m_iCurrentIndex;
				eReturnStatus = RUNNING;
			}
		}
		else
		{
			eReturnStatus = eStatus;
		}
	}

	if( eReturnStatus == SUCCESS
		|| eReturnStatus == FAILURE )
	{
		Reset();
	}

	return eReturnStatus;
}