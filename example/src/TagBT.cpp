// This file's header.
#include "TagBT.h"

#include "Selector.h"
#include "IsRightStatus.h"
#include "Inverter.h"
#include "IsEnemyInSight.h"
#include "LoopUntilFailure.h"
#include "Sequence.h"
#include "Chase.h"
#include "Seek.h"
#include "RunAway.h"
#include "Flee.h"
#include "Sleep.h"

TagBT::TagBT(AIBrain* pxOwnerBrain) : BehaviourTree(pxOwnerBrain)
{
	//Behaviour actions
	m_pxChase = new Chase(GetOwnerBrain());
	m_pxSeek = new Seek(GetOwnerBrain());
	m_pxRunAway = new RunAway(GetOwnerBrain());
	m_pxFlee = new Flee(GetOwnerBrain());
	m_pxSleep = new Sleep(GetOwnerBrain());

	//Enemy sight checkers
	m_pxIsEnemyInSightChase = new IsEnemyInSight(GetOwnerBrain());
	m_pxIsEnemyInSightSeek = new IsEnemyInSight(GetOwnerBrain());
	m_pxIsEnemyInSightRunAway = new IsEnemyInSight(GetOwnerBrain());
	m_pxIsEnemyInSightFlee = new IsEnemyInSight(GetOwnerBrain());

	//Inverters for enemy in sight
	m_pxInverterSeek = new Inverter(m_pxIsEnemyInSightSeek);
	m_pxInverterFlee = new Inverter(m_pxIsEnemyInSightFlee);

	//Sequence for correct behaviour
	m_pxIsChaseSequence = new Sequence();
	m_pxIsChaseSequence->AddChild(m_pxIsEnemyInSightChase);
	m_pxIsChaseSequence->AddChild(m_pxChase);
	m_pxIsSeekSequence = new Sequence();
	m_pxIsSeekSequence->AddChild(m_pxInverterSeek);
	m_pxIsSeekSequence->AddChild(m_pxSeek);
	m_pxIsRunAwaySequence = new Sequence();
	m_pxIsRunAwaySequence->AddChild(m_pxIsEnemyInSightRunAway);
	m_pxIsRunAwaySequence->AddChild(m_pxRunAway);
	m_pxIsFleeSequence = new Sequence();
	m_pxIsFleeSequence->AddChild(m_pxInverterFlee);
	m_pxIsFleeSequence->AddChild(m_pxFlee);

	//Right state checkers
	m_pxIsChase = new IsRightStatus(GetOwnerBrain(), CHASE);
	m_pxIsSeek = new IsRightStatus(GetOwnerBrain(), SEEK);
	m_pxIsRunAway = new IsRightStatus(GetOwnerBrain(), RUNAWAY);
	m_pxIsFlee = new IsRightStatus(GetOwnerBrain(), FLEE);
	m_pxIsSleep = new IsRightStatus(GetOwnerBrain(), SLEEP);

	//Sequences for whole behaviours
	m_pxChaseSequence = new Sequence();
	m_pxChaseSequence->AddChild(m_pxIsChase);
	m_pxChaseSequence->AddChild(m_pxIsChaseSequence);
	m_pxSeekSequence = new Sequence();
	m_pxSeekSequence->AddChild(m_pxIsSeek);
	m_pxSeekSequence->AddChild(m_pxIsSeekSequence);
	m_pxRunAwaySequence = new Sequence();
	m_pxRunAwaySequence->AddChild(m_pxIsRunAway);
	m_pxRunAwaySequence->AddChild(m_pxIsRunAwaySequence);
	m_pxFleeSequence = new Sequence();
	m_pxFleeSequence->AddChild(m_pxIsFlee);
	m_pxFleeSequence->AddChild(m_pxIsFleeSequence);
	m_pxSleepSequence = new Sequence();
	m_pxSleepSequence->AddChild(m_pxIsSleep);
	m_pxSleepSequence->AddChild(m_pxSleep);

	//Root selector
	m_pxRootSelector = new Selector();
	m_pxRootSelector->AddChild(m_pxChaseSequence);
	m_pxRootSelector->AddChild(m_pxSeekSequence);
	m_pxRootSelector->AddChild(m_pxRunAwaySequence);
	m_pxRootSelector->AddChild(m_pxFleeSequence);
	m_pxRootSelector->AddChild(m_pxSleepSequence);

}

TagBT::~TagBT()
{
	delete m_pxRootSelector;

	delete m_pxChaseSequence;
	delete m_pxIsChase;
	delete m_pxIsChaseSequence;
	delete m_pxIsEnemyInSightChase;
	delete m_pxChase;

	delete m_pxSeekSequence;
	delete m_pxIsSeek;
	delete m_pxIsSeekSequence;
	delete m_pxInverterSeek;
	delete m_pxIsEnemyInSightSeek;
	delete m_pxSeek;

	delete m_pxRunAwaySequence;
	delete m_pxIsRunAway;
	delete m_pxIsRunAwaySequence;
	delete m_pxIsEnemyInSightRunAway;
	delete m_pxRunAway;

	delete m_pxFleeSequence;
	delete m_pxIsFlee;
	delete m_pxIsFleeSequence;
	delete m_pxInverterFlee;
	delete m_pxIsEnemyInSightFlee;
	delete m_pxFlee;

	delete m_pxIsSleep;
	delete m_pxSleepSequence;
	delete m_pxSleep;
}

void TagBT::Initialise()
{

}

void TagBT::Update()
{
	m_pxRootSelector->Update();
}

