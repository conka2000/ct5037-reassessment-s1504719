#ifndef __Application_H_
#define __Application_H_

#include "Application.h"
#include <glm/glm.hpp>
#include "AIActor.h"
#include "Stage.h"
#include "ChargeArea.h"

// Derived application class that wraps up all globals neatly
class MyApplication : public Application
{
public:

	MyApplication();
	virtual ~MyApplication();

	static ChargeArea s_xChargeArea;
	Stage m_xStage;
	static float FindDistanceBetween(glm::vec3 xPos1, glm::vec3 xPos2);

protected:

	virtual bool onCreate();
	virtual void Update(float a_deltaTime);
	virtual void Draw();
	virtual void Destroy();

	glm::mat4	m_cameraMatrix;
	glm::mat4	m_projectionMatrix;

	//Actors
	AIActor m_xOurActor;
	AIActor m_xOurActor2;

	NavMesh m_xNavMesh; //Navmesh
	
};

#endif // __Application_H_