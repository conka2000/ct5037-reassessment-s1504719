#pragma once

#ifndef INVERTER
#define INVERTER

#include "Decorator.h"

class Inverter : public Decorator
{
public:
	Inverter(Node* pxChild);

	NODE_STATUS Update() override;
};

#endif // INVERTER