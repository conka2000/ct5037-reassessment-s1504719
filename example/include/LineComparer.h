#pragma once

#ifndef LINECOMPARER
#define LINECOMPARER

#include "Gizmos.h"
#include "Utilities.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>

//Used to see if lines intersect eachother
class LineComparer
{
public:
	LineComparer();
	~LineComparer();

	bool CCW(glm::vec3 point1, glm::vec3 point2, glm::vec3 point3);
	bool LinesIntersect(glm::vec3 line1point1, glm::vec3 line1point2, glm::vec3 line2point1, glm::vec3 line2point2);
};

#endif LINECOMPARER
