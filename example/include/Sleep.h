#pragma once

#ifndef SLEEP_ACTION
#define SLEEP_ACTION

#include "Action.h"
#include <glm\vec3.hpp>

//Actor sleeps for 10 seconds before awaking and chasing other actor
class Sleep : public Action
{
public:
	Sleep(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;

private:
	float sleepTime = 10.0f;
	float sleepTimer = 0.f;
};

#endif // SLEEP_ACTION