#pragma once

#pragma once

#ifndef LOOPUNTILFAILURE
#define LOOPUNTILFAILURE

#include "Decorator.h"

class LoopUntilFailure : public Decorator
{
public:
	LoopUntilFailure(Node* pxChild);

	NODE_STATUS Update() override;
};

#endif // LOOPUNTILFAILURE
