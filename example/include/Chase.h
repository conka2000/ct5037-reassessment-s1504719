#pragma once

#ifndef CHASE_ACTION
#define CHASE_ACTION

#include "Action.h"
#include <glm\vec3.hpp>

//Actor directly chases enemy
class Chase : public Action
{
public:
	Chase(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;

private:
	bool m_bIsMoving;
};

#endif // CHASE_ACTION