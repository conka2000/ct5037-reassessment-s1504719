#pragma once

#include <glm/ext.hpp>
#include "AIBrain.h"
#include "NavMesh.h"

//Actor can be in any one of these states at a time
enum BEHAVIOUR_STATE
{
	CHASE, //Chase while seeing
	SEEK, //Chase while not seeing
	RUNAWAY, //Run while seeing
	FLEE, //Run while not seeing
	SLEEP //Sleep when tagged
};

class AIActor
{
public:
	AIActor();
	~AIActor();

	void Start(bool tagged, glm::vec3 startPos, AIActor* enemy);

	void Update(float a_deltaTime);

	void RequestMove(const glm::vec3& xPosition);
	void RequestStop();

	//Set attributes
	void SetNavMesh(NavMesh* theMesh);
	void SetState(BEHAVIOUR_STATE theState);
	void SetPreviousNode(int preNode) { previousNode = preNode; }

	void Tagged();

	//Get attributes
	const glm::vec3& GetCurrentPosition() const { return m_xCurrentPosition; }
	const glm::vec3& GetMoveToPosition() const { return m_xMoveToPosition; }
	const glm::vec3& GetMoveToDirection() const { return m_xMoveToDirection; }
	const glm::vec4& GetCurrentColour() const { return m_xCurrentColour; }
	const float GetCurrentCharge() const { return m_fCurrentEnergy; }
	const BEHAVIOUR_STATE GetCurrentState() const { return currentState; }
	NavMesh* GetNavMesh() { return mapMesh; }
	AIActor* GetEnemyActor() { return m_xEnemy; }
	int GetCurrentNodeID() { 
		int myID = mapMesh->GetNodeQuadIDOfPoint(m_xCurrentPosition);
		return myID; 
	}
	int GetEnemyCurrentNodeID() { 
		int otherID = mapMesh->GetNodeQuadIDOfPoint(m_xEnemy->GetCurrentPosition());
		return otherID;
	}
	int GetPreviousNode() { return previousNode; }

private:
	glm::vec3 m_xCurrentPosition;
	glm::vec3 m_xMoveToPosition;
	glm::vec3 m_xMoveToDirection;
	glm::vec4 m_xCurrentColour;

	float sleepTimer = 0.f;

	float m_fCurrentEnergy;
	bool m_bMoveRequested;

	AIBrain m_xBrain;

	AIActor* m_xEnemy;

	NavMesh* mapMesh;

	BEHAVIOUR_STATE currentState;

	float speed = 0.1f;

	int previousNode = -1;
};