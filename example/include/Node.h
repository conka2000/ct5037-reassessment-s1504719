#pragma once

#ifndef NODE
#define NODE

enum NODE_STATUS
{
	SUCCESS,
	FAILURE,
	RUNNING,
	NONE
};

// NODE ///////////////////////////////////////////////////////////////////////
class Node
{
public:
	Node() : m_eNodeStatus( NONE ) {}

	virtual NODE_STATUS Update() = 0;

private:
	NODE_STATUS m_eNodeStatus;
};
///////////////////////////////////////////////////////////////////////////////

#endif // NODE