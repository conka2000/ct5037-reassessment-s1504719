#pragma once

#ifndef RUNAWAY_ACTION
#define RUNAWAY_ACTION

#include "Action.h"
#include <glm\vec3.hpp>

//Actor runs away from other actor when it can see them
class RunAway : public Action
{
public:
	RunAway(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;

private:
	glm::vec3 goToPos;
	bool m_bIsMoving;

	int goToNode;
};

#endif // RUNAWAY_ACTION