#pragma once
#include <ctime>

// Forward declarations.
class AIActor;
class BehaviourTree;

class AIBrain
{
public:
	AIBrain( AIActor* pxOwnerActor );
	~AIBrain();

	void Update(float a_deltaTime);

	AIActor* GetOwnerActor() const { return m_pxOwnerActor; }

private:
	AIActor* m_pxOwnerActor;
	BehaviourTree* m_pxCurrentBT;
};

