#pragma once

#ifndef CONDITION
#define CONDITION

#include "Node.h"

class AIBrain;

class Condition : public Node
{
public:
	Condition(AIBrain* pxOwner) : m_pxOwnerBrain(pxOwner) {}

	NODE_STATUS Update() override = 0;

	AIBrain* GetOwnerBrain() { return m_pxOwnerBrain; }

private:
	AIBrain* m_pxOwnerBrain;
};

#endif // CONDITION