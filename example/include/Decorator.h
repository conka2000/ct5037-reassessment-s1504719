#pragma once

#ifndef DECORATOR
#define DECORATOR

#include "Node.h"

class Decorator : public Node
{
public:
	Decorator(Node* pxChild) : m_pxChild(pxChild) {};

	NODE_STATUS Update() override = 0;

	Node* GetChildNode() { return m_pxChild; };
private:
	Node* m_pxChild;
};


#endif // DECORATOR