#pragma once

#ifndef RETURN_FOR_RECHARGE
#define RETURN_FOR_RECHARGE

#include "Action.h"

class Recharge : public Action
{
public:
	Recharge( AIBrain* pxOwnerBrain );

	NODE_STATUS Update() override;

private:
	bool m_bIsMoving;
};

#endif // RETURN_FOR_RECHARGE