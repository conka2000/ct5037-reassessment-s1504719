#pragma once

#ifndef FLEE_ACTION
#define FLEE_ACTION

#include "Action.h"
#include "LineComparer.h"
#include <glm\vec3.hpp>

//Actor cannot see enemy while running away so "explores"
class Flee : public Action
{
public:
	Flee(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;

private:
	glm::vec3 goToPos;
	bool m_bIsMoving;

	int goToNode;

	LineComparer lineComparer;
};

#endif // FLEE_ACTION