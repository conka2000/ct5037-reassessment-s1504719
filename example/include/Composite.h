#pragma once

#ifndef COMPOSITES
#define COMPOSITES

#include "Node.h"
#include <vector>

class Composite : public Node
{
public:
	const std::vector<Node*>& GetChildNodes() const { return m_xChildrenNodes; }
	void AddChild(Node* m_xNewChild) { m_xChildrenNodes.push_back(m_xNewChild); }
protected:
	void Reset() { m_iCurrentIndex = 0; }
	int m_iCurrentIndex;
private:
	std::vector<Node*> m_xChildrenNodes;
};

#endif // COMPOSITES