#pragma once

#ifndef IS_ENEMY_IN_SIGHT
#define IS_ENEMY_IN_SIGHT

#include "Condition.h"
#include "MyApplication.h"
#include "LineComparer.h"

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>

class AIBrain;

//Checks if enemy is visible (no wall inbetween)
class IsEnemyInSight : public Condition
{
public:
	IsEnemyInSight(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;

	glm::vec3 pxWallLines[20];

	LineComparer lineComparer;

	Stage mapStage; //Copy of stage for wall data
};

#endif // IS_ENEMY_IN_SIGHT
