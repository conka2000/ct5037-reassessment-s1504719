#pragma once

#ifndef MOVE_TO_CORNER
#define MOVE_TO_CORNER

#include "Action.h"
#include <glm\vec3.hpp>

class Patrol : public Action
{
public:
	Patrol( AIBrain* pxOwnerBrain );

	NODE_STATUS Update() override;

private:
	bool m_bIsMoving;
	int m_iCurrentCornerIndex;
};

#endif // MOVE_TO_CORNER