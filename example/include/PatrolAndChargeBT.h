#pragma once
#ifndef PATROL_AND_CHARGE_BT
#define PATROL_AND_CHARGE_BT

#include "BehaviourTree.h"

class Selector;
class IsChargeLow;
class Inverter;
class Recharge;
class Sequence;
class Patrol;

class LoopUntilFailure;

class PatrolAndChargeBT : public BehaviourTree
{
public:
	PatrolAndChargeBT(AIBrain* pxOwnerBrain);
	~PatrolAndChargeBT();

	void Initialise() override;

	void Update() override;

private:
	Selector* m_pxRootSelector;
	IsChargeLow* m_pxIsChargeLow;
	Inverter* m_pxInverter;
	Recharge* m_pxRecharge;
	Sequence* m_pxMoveSequence;
	Patrol* m_pxPatrol;

	LoopUntilFailure* m_pxLoopUntilFailure;
};

#endif // PATROL_AND_CHARGE_BT