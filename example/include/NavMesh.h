#pragma once

#ifndef NAVMESH
#define NAVMESH

#include "Gizmos.h"
#include "Utilities.h"
#include "LineComparer.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <glm/ext.hpp>
#include <iostream>
#include <stack>
#include <deque>

//The information held in a node on the navmesh
struct NavNodeQuad
{
	glm::vec3 position; //Center of node
	glm::vec3 vertices[4]; //Points of rectangle
	int neighbours[4] = {-1,-1,-1,-1}; //IDs of neighbours to node
	int navMeshID; //Personal ID of node
	int neighbourCount = 0; //Amount of neighbours the node has
};

//Navmesh nodes are square
class NavMesh
{
public:
	NavMesh();
	~NavMesh();

	void OnCreate();
	void Update(float a_deltaTime);
	void AddQuadNode(glm::vec3 q_Vertices[4]);
	
	int GetNodeQuadIDOfPoint(glm::vec3 p_Position);

	glm::vec4 GetNeighboursOf(int navID);
	int GetNeighbourCountOf(int navID);
	glm::vec3 GetPointToNextNode(int myID, int otherID);
	
private:
	std::deque<NavNodeQuad> myNodes; //List holding nodes
	int nodeCount = 0;

	LineComparer lineComparer;
};

#endif