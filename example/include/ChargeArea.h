#pragma once
#include <glm/ext.hpp>

class ChargeArea
{
public:
	ChargeArea();
	~ChargeArea();

	void Update();

	glm::vec3 GetPosition() { return m_xPosition; }

private:
	glm::vec3 m_xPosition;
	glm::vec4 m_xColour;

};

