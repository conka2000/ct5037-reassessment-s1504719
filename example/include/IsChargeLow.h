#pragma once

#ifndef IS_CHARGE_LOW
#define IS_CHARGE_LOW

#include "Condition.h"

class AIBrain;

class IsChargeLow : public Condition
{
public:
	IsChargeLow(AIBrain* pxOwnerBrain);

	NODE_STATUS Update() override;
};

#endif // IS_CHARGE_LOW